package net.degoes.zio

import zio._

object ForkJoin extends App {
  import zio.Console._

  val printer =
    printLine(".").repeat(Schedule.recurs(10))

  /**
   * EXERCISE
   *
   * Using `ZIO#fork`, fork the `printer` into a separate fiber, and then
   * print out a message, "Forked", then join the fiber using `Fiber#join`,
   * and finally, print out a message "Joined".
   */
  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] =
    (for {
      f <- printer.fork
      _ <- printLine("Forked")
      _ <- f.join
      _ <- printLine("Joined")
    } yield ()).exitCode
}

object ForkInterrupt extends App {
  import zio.Console._

  val infinitePrinter =
    printLine(".").forever

  /**
   * EXERCISE
   *
   * Using `ZIO#fork`, fork the `printer` into a separate fiber, and then
   * print out a message, "Forked", then using `ZIO.sleep`, sleep for 100
   * milliseconds, then interrupt the fiber using `Fiber#interrupt`, and
   * finally, print out a message "Interrupted".
   */
  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] =
    (for {
      f <- infinitePrinter.fork
      _ <- printLine("Forked")
      _ <- ZIO.sleep(100.millis)
      _ <- f.interrupt
      _ <- printLine("Interrupted")
    } yield ()).exitCode
}

object ParallelFib extends App {
  import zio.Console._

  /**
   * EXERCISE
   *
   * Rewrite this implementation to compute nth fibonacci number in parallel.
   */
  def fib(n: Int): UIO[BigInt] = {
    def loop(n: Int): UIO[BigInt] =
      if (n <= 1) UIO(n)
      else
        for {
          f1 <- loop(n - 1).fork
          f2 <- loop(n - 2).fork
          r1 <- f1.join
          r2 <- f2.join
        } yield r1 + r2

    loop(n)
  }

  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] =
    (for {
      _ <- printLine(
            "What number of the fibonacci sequence should we calculate?"
          )
      n <- readLine.orDie.flatMap(input => ZIO(input.toInt)).eventually
      f <- fib(n)
      _ <- printLine(s"fib($n) = $f")
    } yield ()).exitCode
}

object AlarmAppImproved extends App {
  import zio.Console._

  import java.io.IOException
  import java.util.concurrent.TimeUnit

  lazy val getAlarmDuration: ZIO[Has[Console], IOException, Duration] = {
    def parseDuration(input: String): IO[NumberFormatException, Duration] =
      ZIO
        .attempt(
          Duration((input.toDouble * 1000.0).toLong, TimeUnit.MILLISECONDS)
        )
        .refineToOrDie[NumberFormatException]

    val fallback = printLine("You didn't enter a number of seconds!") *> getAlarmDuration

    for {
      _        <- printLine("Please enter the number of seconds to sleep: ")
      input    <- readLine
      duration <- parseDuration(input) orElse fallback
    } yield duration
  }

  val tick = ZIO.sleep(1.second) *> printLine(".")

  /**
   * EXERCISE
   *
   * Create a program that asks the user for a number of seconds to sleep,
   * sleeps the specified number of seconds using ZIO.sleep(d), concurrently
   * prints a dot every second that the alarm is sleeping for, and then
   * prints out a wakeup alarm message, like "Time to wakeup!!!".
   */
  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] =
    (for {
      d <- getAlarmDuration
      f <- tick.forever.fork
      _ <- ZIO.sleep(d)
      _ <- f.interrupt
      _ <- printLine("Time to wakeup!!!")
    } yield ()).exitCode
}

/**
 * Effects can be forked to run in separate fibers. Sharing information between fibers can be done
 * using the `Ref` data type, which is like a concurrent version of a Scala `var`.
 */
object ComputePi extends App {
  import zio.Console._
  import zio.Random._

  import java.io.IOException

  /**
   * Some state to keep track of all points inside a circle,
   * and total number of points.
   */
  final case class PiState(
    inside: Ref[Long],
    total: Ref[Long]
  )

  /**
   * A function to estimate pi.
   */
  def estimatePi(inside: Long, total: Long): Double =
    (inside.toDouble / total.toDouble) * 4.0

  /**
   * A helper function that determines if a point lies in
   * a circle of 1 radius.
   */
  def insideCircle(x: Double, y: Double): Boolean =
    Math.sqrt(x * x + y * y) <= 1.0

  /**
   * An effect that computes a random (x, y) point.
   */
  val randomPoint: URIO[Has[Random], (Double, Double)] =
    nextDouble zip nextDouble

  def generator(state: PiState, n: Int): URIO[Has[Random], Unit] =
    (for {
      point    <- randomPoint
      isInside = (insideCircle _).tupled(point)
      _        <- state.total.update(_ + 1)
      _        <- state.inside.update(_ + 1).when(isInside)
    } yield ()).repeatN(n)

  def getPiValue(state: PiState): URIO[Has[Console], Double] =
    for {
      inside <- state.inside.get
      total  <- state.total.get
    } yield estimatePi(inside, total)

  def printer(state: PiState): ZIO[Has[Clock] with Has[Console], IOException, Unit] =
    (for {
      pi <- getPiValue(state)
      _  <- printLine(s"current estimated Pi value is $pi")
      _  <- ZIO.sleep(100.millis)
    } yield ()).forever

  /**
   * EXERCISE
   *
   * Build a multi-fiber program that estimates the value of `pi`. Print out
   * ongoing estimates continuously until the estimation is complete.
   */
  def run(args: List[String]): URIO[ZEnv, ExitCode] =
    (for {
      insideRef  <- ZRef.make(0L)
      totalRef   <- ZRef.make(0L)
      state      = PiState(insideRef, totalRef)
      gen        = generator(state, 100000)
      generators = ZIO.collectAllParDiscard(List.fill(4)(gen))
      _          <- printer(state) race generators
      finalPi    <- getPiValue(state)
      _          <- printLine(s"final Pi value is $finalPi")
    } yield ()).exitCode
}

object ParallelZip extends App {
  import zio.Console._

  def fib(n: Int): UIO[Int] =
    if (n <= 1) UIO(n)
    else
      UIO.suspendSucceed {
        (fib(n - 1) zipWith fib(n - 2))(_ + _)
      }

  /**
   * EXERCISE
   *
   * Compute fib(10) and fib(13) in parallel using `ZIO#zipPar`, and display
   * the result.
   */
  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] =
    (fib(10) zipPar fib(13)).flatMap {
      case (f10, f13) =>
        printLine(s"fib(10) = $f10, fib(13) = $f13")
    }.exitCode
}

object StmSwap extends App {
  import zio.Console._
  import zio.stm._

  /**
   * EXERCISE
   *
   * Demonstrate the following code does not reliably swap two values in the
   * presence of concurrency.
   */
  def exampleRef: UIO[Int] = {
    def swap[A](ref1: Ref[A], ref2: Ref[A]): UIO[Unit] =
      for {
        v1 <- ref1.get
        v2 <- ref2.get
        _  <- ref2.set(v1)
        _  <- ref1.set(v2)
      } yield ()

    for {
      ref1   <- Ref.make(100)
      ref2   <- Ref.make(0)
      fiber1 <- swap(ref1, ref2).repeatN(100).fork
      fiber2 <- swap(ref2, ref1).repeatN(100).fork
      _      <- (fiber1 zip fiber2).join
      value  <- (ref1.get zipWith ref2.get)(_ + _)
    } yield value
  }

  /**
   * EXERCISE
   *
   * Using `STM`, implement a safe version of the swap function.
   */
  def exampleStm: UIO[Int] = {
    def swap[A](ref1: TRef[A], ref2: TRef[A]): USTM[Unit] =
      for {
        v1 <- ref1.get
        v2 <- ref2.get
        _  <- ref2.set(v1)
        _  <- ref1.set(v2)
      } yield ()

    for {
      ref1       <- TRef.makeCommit(100)
      ref2       <- TRef.makeCommit(0)
      atomicSwap = swap(ref1, ref2).commit
      fiber1     <- atomicSwap.repeatN(1000).fork
      fiber2     <- atomicSwap.repeatN(1000).fork
      _          <- (fiber1 zip fiber2).join
      value      <- (ref1.get zipWith ref2.get)(_ + _).commit
    } yield value
  }

  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] =
    exampleStm.map(_.toString).flatMap(printLine(_)).exitCode
}

object StmLock extends App {
  import zio.Console._
  import zio.stm._

  /**
   * EXERCISE
   *
   * Using STM, implement a simple binary lock by implementing the creation,
   * acquisition, and release methods.
   */
  class Lock private (tref: TRef[Boolean]) {
    def acquire: UIO[Unit] =
      STM.atomically(tref.get.retryUntil(identity) *> tref.set(false))

    def release: UIO[Unit] =
      STM.atomically(tref.get.retryWhile(identity) *> tref.set(true))
  }
  object Lock {
    def make: UIO[Lock] = TRef.makeCommit(true).map(new Lock(_))
  }

  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] =
    (for {
      lock <- Lock.make
      fiber1 <- lock.acquire
                 .acquireRelease(lock.release)(printLine("Bob  : I have the lock!"))
                 .repeat(Schedule.recurs(10))
                 .fork
      fiber2 <- lock.acquire
                 .acquireRelease(lock.release)(printLine("Sarah: I have the lock!"))
                 .repeat(Schedule.recurs(10))
                 .fork
      _ <- (fiber1 zip fiber2).join
    } yield ()).exitCode
}

object StmQueue extends App {
  import zio.Console._
  import zio.stm._

  import scala.collection.immutable.{Queue => ScalaQueue}

  /**
   * EXERCISE
   *
   * Using STM, implement a async queue with double back-pressuring.
   */
  class Queue[A] private (capacity: Int, queue: TRef[ScalaQueue[A]]) {
    def take: UIO[A] =
      (queue.get.retryWhile(_.isEmpty) *> queue.modify(_.dequeue)).commit

    def offer(a: A): UIO[Unit] =
      (queue.get.retryWhile(_.length >= capacity) *> queue.update(_.enqueue(a))).commit
  }

  object Queue {
    def bounded[A](capacity: Int): UIO[Queue[A]] =
      TRef.make(ScalaQueue.empty[A]).map(new Queue[A](capacity, _)).commit
  }

  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] =
    (for {
      queue <- Queue.bounded[Int](10)
      _     <- ZIO.foreach(0 to 100)(i => queue.offer(i) *> printLine(s"Offered: $i")).fork
      _     <- ZIO.foreach(0 to 100)(_ => queue.take.flatMap(i => printLine(s"Got: $i")))
    } yield ()).exitCode
}

object StmLunchTime extends App {
  import zio.Console._
  import zio.stm._

  /**
   * EXERCISE
   *
   * Using STM, implement the missing methods of Attendee.
   */
  final case class Attendee(index: Int, state: TRef[Attendee.State]) {
    import Attendee.State._

    def isStarving: USTM[Boolean] = state.get.map(_ == Starving)

    def feed: USTM[Unit] = state.set(Full)
  }
  object Attendee {
    sealed trait State
    object State {
      case object Starving extends State
      case object Full     extends State
    }
  }

  /**
   * EXERCISE
   *
   * Using STM, implement the missing methods of Table.
   */
  final case class Table(seats: TArray[Boolean]) {
    def findEmptySeat: USTM[Option[Int]] =
      seats
        .fold[(Int, Option[Int])]((0, None)) {
          case ((index, z @ Some(_)), _) => (index + 1, z)
          case ((index, None), taken) =>
            (index + 1, if (taken) None else Some(index))
        }
        .map(_._2)

    def takeSeat(index: Int): USTM[Unit] = seats.update(index, _ => true)

    def vacateSeat(index: Int): USTM[Unit] = seats.update(index, _ => false)
  }

  /**
   * EXERCISE
   *
   * Using STM, implement a method that feeds a single attendee.
   */
  def feedAttendee(t: Table, a: Attendee): USTM[Int] =
    for {
      seat <- t.findEmptySeat.collect { case Some(i) => i }
      _    <- t.takeSeat(seat)
      _    <- a.feed
    } yield seat

  /**
   * EXERCISE
   *
   * Using STM, implement a method that feeds only the starving attendees.
   */
  def feedStarving(table: Table, attendees: Iterable[Attendee]): URIO[Has[Clock] with Has[Console], Unit] =
    ZIO.foreachParDiscard(attendees) { a =>
      for {
        optSeat <- a.isStarving.flatMap { starving =>
                    if (starving)
                      feedAttendee(table, a).map(Some(_))
                    else
                      STM.succeed(None)
                  }.commit
        _ <- optSeat.fold[URIO[Has[Clock] with Has[Console], Unit]](ZIO.unit) { seat =>
              printLine(s"feeding attendee ${a.index} at seat $seat").orDie *>
                ZIO.sleep(100.millis) *>
                table.vacateSeat(seat).commit
            }
      } yield ()
    }

  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] = {
    val Attendees = 100
    val TableSize = 5

    for {
      attendees <- ZIO.foreach(0 to Attendees) { i =>
                    TRef
                      .make[Attendee.State](Attendee.State.Starving)
                      .map(Attendee(i, _))
                      .commit
                  }
      table <- TArray
                .fromIterable(List.fill(TableSize)(false))
                .map(Table)
                .commit
      _ <- feedStarving(table, attendees)
    } yield ExitCode.success
  }
}

object StmPriorityQueue extends App {
  import zio.Console._
  import zio.stm._

  /**
   * EXERCISE
   *
   * Using STM, design a priority queue, where smaller integers are assumed
   * to have higher priority than greater integers.
   */
  class PriorityQueue[A] private (
    minLevel: TRef[Option[Int]],
    map: TMap[Int, TQueue[A]]
  ) {
    def offer(a: A, priority: Int): USTM[Unit] =
      for {
        optQueue <- map.get(priority)
        _ <- optQueue match {
              case Some(queue) =>
                queue.offer(a)
              case None =>
                TQueue.unbounded[A].flatMap(q => q.offer(a) *> map.put(priority, q))
            }
        _ <- minLevel.update {
              case None =>
                Some(priority)
              case Some(level) =>
                Some(level min priority)
            }
      } yield ()

    def take: USTM[A] = {
      val getMinLevel: USTM[Option[Int]] =
        map.foldSTM[Option[Int], Nothing](None) {
          case (None, (k, v)) =>
            v.isEmpty.map(empty => if (empty) None else Some(k))
          case (Some(level), (k, v)) =>
            v.isEmpty.map(empty => if (empty) Some(level) else Some(level min k))
        }

      for {
        level    <- minLevel.get.collect { case Some(level) => level }
        optQueue <- map.get(level)
        a        <- optQueue.get.take
        newLevel <- getMinLevel
        _        <- minLevel.set(newLevel)
      } yield a
    }

  }
  object PriorityQueue {
    def make[A]: USTM[PriorityQueue[A]] =
      for {
        minLevel <- TRef.make[Option[Int]](None)
        map      <- TMap.empty[Int, TQueue[A]]
      } yield new PriorityQueue[A](minLevel, map)
  }

  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] =
    (for {
      _     <- printLine("Enter any key to exit...")
      queue <- PriorityQueue.make[String].commit
      lowPriority = ZIO.foreach(1 to 10) { i =>
        ZIO.sleep(10.millis) *> queue
          .offer(s"Offer: ${i} with priority 3", 3)
          .commit
      }
      midPriority = ZIO.foreach(1 to 10) { i =>
        ZIO.sleep(20.millis) *> queue
          .offer(s"Offer: ${i} with priority 1", 1)
          .commit
      }
      highPriority = ZIO.foreach(1 to 10) { i =>
        ZIO.sleep(30.millis) *> queue
          .offer(s"Offer: ${i} with priority 0", 0)
          .commit
      }
      _ <- ZIO.forkAllDiscard(List(lowPriority, midPriority, highPriority)) *>
            queue.take.commit
              .flatMap(printLine(_))
              .forever
              .fork *>
            readLine
    } yield 0).exitCode
}

object StmReentrantLock extends App {
  import zio.Clock._
  import zio.Console._
  import zio.stm._

  import java.io.IOException

  private final case class WriteLock(
    writeCount: Int,
    readCount: Int,
    fiberId: Fiber.Id
  )
  private final class ReadLock private (readers: Map[Fiber.Id, Int]) {
    def total: Int = readers.values.sum

    def noOtherHolder(fiberId: Fiber.Id): Boolean =
      readers.isEmpty || (readers.size == 1 && readers.contains(fiberId))

    def readLocks(fiberId: Fiber.Id): Int =
      readers.get(fiberId).fold(0)(identity)

    def adjust(fiberId: Fiber.Id, adjust: Int): ReadLock = {
      val total = readLocks(fiberId)

      val newTotal = total + adjust

      new ReadLock(
        readers =
          if (newTotal == 0) readers - fiberId
          else readers.updated(fiberId, newTotal)
      )
    }
  }
  private object ReadLock {
    val empty: ReadLock = new ReadLock(Map())

    def apply(fiberId: Fiber.Id, count: Int): ReadLock =
      if (count <= 0) empty else new ReadLock(Map(fiberId -> count))
  }

  /**
   * EXERCISE
   *
   * Using STM, implement a reentrant read/write lock.
   */
  class ReentrantReadWriteLock(data: TRef[Either[ReadLock, WriteLock]]) {
    def writeLocks: UIO[Int] = data.get.map(_.fold(_ => 0, _.writeCount)).commit

    def writeLocked: UIO[Boolean] = writeLocks.map(_ > 0)

    def readLocks: UIO[Int] = data.get.map(_.fold(_.total, _.readCount)).commit

    def readLocked: UIO[Boolean] = readLocks.map(_ > 0)

    val read: Managed[Nothing, Int] =
      ZIO.fiberId.toManaged.flatMap { fiberId =>
        val acquire: UIO[Int] = data.get.flatMap {
          case Left(rl) =>
            val newLock = rl.adjust(fiberId, 1)
            STM.succeed((Left(newLock), newLock.readLocks(fiberId)))

          case Right(wl) if wl.fiberId == fiberId =>
            val newLock = wl.copy(readCount = wl.readCount + 1)
            STM.succeed((Right(newLock), newLock.readCount))

          case Right(_) =>
            data.get.collect {
              case Left(rl) =>
                val newLock = rl.adjust(fiberId, 1)
                (Left(newLock), newLock.readLocks(fiberId))
            }
        }.flatMap {
          case (lock, level) =>
            data.set(lock).as(level)
        }.commit

        val release: UIO[Unit] = data.get.map {
          case Left(rl) =>
            Left(rl.adjust(fiberId, -1))
          case Right(wl) if wl.fiberId == fiberId =>
            Right(wl.copy(readCount = wl.readCount - 1))
          case Right(wl) =>
            println("WARNING: unreachable code")
            Right(wl)
        }.flatMap(data.set).commit

        ZManaged.acquireRelease(acquire)(release)
      }

    val write: Managed[Nothing, Int] =
      ZIO.fiberId.toManaged.flatMap { fiberId =>
        val acquire: UIO[Int] = data.get.flatMap {
          case Right(wl) if wl.fiberId == fiberId =>
            val newLock = wl.copy(writeCount = wl.writeCount + 1)
            STM.succeed((Right(newLock), newLock.writeCount))

          case _ =>
            data.get.collect {
              case Left(rl) if rl.noOtherHolder(fiberId) =>
                val newLock = WriteLock(writeCount = 1, readCount = rl.readLocks(fiberId), fiberId)
                (Right(newLock), 1)
            }
        }.flatMap {
          case (lock, level) =>
            data.set(lock).as(level)
        }.commit

        val release: UIO[Unit] = data.get.map {
          case Right(wl) if wl.fiberId == fiberId =>
            if (wl.writeCount == 1 && wl.readCount == 0)
              Left(ReadLock.empty)
            else
              Right(wl.copy(writeCount = wl.writeCount - 1))
        }.flatMap(data.set).commit

        ZManaged.acquireRelease(acquire)(release)
      }
  }
  object ReentrantReadWriteLock {
    def make: UIO[ReentrantReadWriteLock] =
      TRef
        .make[Either[ReadLock, WriteLock]](Left(ReadLock.empty))
        .map(new ReentrantReadWriteLock(_))
        .commit
  }

  def reader(index: Int, lock: ReentrantReadWriteLock, nesting: Int): ZIO[ZEnv, IOException, Unit] =
    if (nesting == 0)
      ZIO.unit
    else
      lock.read.use { level =>
        printLine(s"reader $index entered >> $level level") *>
          sleep(100.millis) *>
          reader(index, lock, nesting - 1) *>
          printLine(s"reader $index << leaving $level level")
      }

  def writer(index: Int, lock: ReentrantReadWriteLock): ZIO[ZEnv, IOException, Unit] =
    lock.read.use { readLevel =>
      printLine(s"writer $index entered >> $readLevel readLevel") *>
        sleep(100.millis) *>
        lock.write.use { writeLevel =>
          printLine(s"writer $index entered >> $writeLevel writeLevel") *>
            sleep(100.millis) *>
            printLine(s"writer $index << leaving $writeLevel writeLevel")
        } *>
        printLine(s"writer $index << leaving $readLevel readLevel")
    }

  def printState(lock: ReentrantReadWriteLock): ZIO[ZEnv, IOException, Unit] =
    (lock.readLocks zip lock.writeLocks).flatMap {
      case (r, w) =>
        printLine(s"LOCK STATE: readers = $r, writers = $w")
    }

  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] =
    (for {
      lock    <- ReentrantReadWriteLock.make
      readers = (1 to 5).toList.map(i => reader(i, lock, i))
      wf1     <- writer(1, lock).fork
      rf      <- ZIO.forkAll(readers)
      _       <- (rf zip wf1).join race (printState(lock) *> sleep(100.millis)).forever
    } yield ()).exitCode
}

object StmDiningPhilosophers extends App {
  import zio.Console._
  import zio.stm._

  import java.io.IOException

  sealed trait Fork
  val Fork = new Fork {}

  final case class Placement(
    left: TRef[Option[Fork]],
    right: TRef[Option[Fork]]
  )

  final case class Roundtable(seats: Vector[Placement])

  /**
   * EXERCISE
   *
   * Using STM, implement the logic of a philosopher to take not one fork, but
   * both forks when they are both available.
   */
  def takeForks(
    left: TRef[Option[Fork]],
    right: TRef[Option[Fork]]
  ): USTM[(Fork, Fork)] =
    for {
      l <- left.get.collect { case Some(fork) => fork }
      r <- right.get.collect { case Some(fork) => fork }
      _ <- left.set(None)
      _ <- right.set(None)
    } yield (l, r)

  /**
   * EXERCISE
   *
   * Using STM, implement the logic of a philosopher to release both forks.
   */
  def putForks(left: TRef[Option[Fork]], right: TRef[Option[Fork]])(
    tuple: (Fork, Fork)
  ): USTM[Unit] =
    for {
      _ <- left.get.retryUntil(_.isEmpty)
      _ <- right.get.retryUntil(_.isEmpty)
      _ <- left.set(Some(tuple._1))
      _ <- right.set(Some(tuple._2))
    } yield ()

  def setupTable(size: Int): UIO[Roundtable] = {
    val makeFork = TRef.make[Option[Fork]](Some(Fork))

    (for {
      allForks0 <- STM.foreach(0 to size)(_ => makeFork)
      allForks  = allForks0 ++ List(allForks0.head)
      placements = (allForks zip allForks.drop(1)).map {
        case (l, r) => Placement(l, r)
      }
    } yield Roundtable(placements.toVector)).commit
  }

  def eat(
    philosopher: Int,
    roundtable: Roundtable
  ): ZIO[Has[Console], IOException, Unit] = {
    val placement = roundtable.seats(philosopher)

    val left  = placement.left
    val right = placement.right

    for {
      forks <- takeForks(left, right).commit
      _     <- printLine(s"Philosopher ${philosopher} eating...")
      _     <- putForks(left, right)(forks).commit
      _     <- printLine(s"Philosopher ${philosopher} is done eating")
    } yield ()
  }

  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] = {
    val count = 10

    def eaters(table: Roundtable): Iterable[ZIO[Has[Console], IOException, Unit]] =
      (0 to count).map(index => eat(index, table))

    (for {
      table <- setupTable(count)
      fiber <- ZIO.forkAll(eaters(table))
      _     <- fiber.join
      _     <- printLine("All philosophers have eaten!")
    } yield ()).exitCode
  }
}
