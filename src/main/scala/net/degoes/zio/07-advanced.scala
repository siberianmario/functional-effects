package net.degoes.zio

import zio.Ref.Synchronized
import zio._
import zio.internal.Executor

import scala.concurrent.ExecutionContext

object PoolLocking extends App {
  import zio.Console._

  lazy val dbPool: Executor = Executor.fromExecutionContext(1024)(ExecutionContext.global)

  /**
   * EXERCISE
   *
   * Using `ZIO#lock`, write an `onDatabase` combinator that runs the
   * specified effect on the database thread pool.
   */
  def onDatabase[R, E, A](zio: ZIO[R, E, A]): ZIO[R, E, A] = zio.lock(dbPool)

  /**
   * EXERCISE
   *
   * Implement a combinator to print out thread information before and after
   * executing the specified effect.
   */
  def threadLogged[R, E, A](zio: ZIO[R, E, A]): ZIO[R, E, A] = {
    val log = ZIO.succeed {
      val thread = Thread.currentThread()

      val id        = thread.getId()
      val name      = thread.getName()
      val groupName = thread.getThreadGroup().getName()

      println(s"Thread($id, $name, $groupName)")
    }

    log *> zio <* log
  }

  /**
   * EXERCISE
   *
   * Use the `threadLogged` combinator around different effects below to
   * determine which threads are executing which effects.
   */
  def run(args: List[String]) =
    (printLine("Main") *>
      threadLogged(onDatabase {
        printLine("Database") *>
          threadLogged(ZIO.blocking {
            printLine("Blocking")
          }) *>
          printLine("Database")
      }) *>
      printLine("Main")).exitCode
}

object PlatformTweaking {
  import Console._
  import zio.internal.Platform

  /**
   * EXERCISE
   *
   * Modify the default platform by specifying a custom behavior for logging errors.
   */
  lazy val platform = Platform.default.withReportFailure { cause: Cause[Any] =>
    scala.Console.err.println(s"ERROR: ${cause.failures}")
  }

  val environment = Runtime.default.environment

  /**
   * EXERCISE
   *
   * Create a custom runtime using `platform` and `environment`, and use this to
   * run an effect.
   */
  lazy val customRuntime: Runtime[ZEnv] = Runtime(environment, platform)

  val effect =
    printLine("Test effect") *>
      ZIO.fail(new RuntimeException("failure"))

  def exampleRun = customRuntime.unsafeRun(effect)

  def main(args: Array[String]): Unit = exampleRun
}

object Sharding extends App {
  import zio.Clock._
  import zio.Console._

  type Worker[R, E, A]        = A => ZIO[R, E, Unit]
  type WorkerBuilder[R, E, A] = Int => Worker[R, E, A]

  /**
   * EXERCISE
   *
   * Create N workers reading from a Queue, if one of them fails, then wait
   * for the other ones to process their current item, but terminate all the
   * workers.
   *
   * Return the first error, or never return, if there is no error.
   */
  def shard[R, E, A](
    queue: Queue[A],
    n: Int,
    worker: WorkerBuilder[R, E, A]
  ): URIO[R, E] = {
    def reader(i: Int, p: Promise[Nothing, E]): URIO[R, Unit] =
      p.isDone.flatMap {
        case true =>
          ZIO.unit
        case false =>
          queue.take.flatMap(worker(i)).catchAll(p.succeed) *> reader(i, p)
      }

    for {
      p      <- Promise.make[Nothing, E]
      _      <- ZIO.forkAll((1 to n).map(reader(_, p)))
      result <- p.await
    } yield result
  }

  def run(args: List[String]) = {
    def makeWorker(ref: Synchronized[Int])(i: Int): Worker[Has[Console] with Has[Clock], String, Int] =
      (work: Int) =>
        ref.updateZIO { count =>
          (if (count < 100) printLine(s"Worker $i is processing item ${work} after ${count}").orDie *> sleep(100.millis)
           else ZIO.fail(s"Uh oh, worker $i failed processing ${work} after ${count}")).as(count + 1)
        }

    (for {
      queue <- Queue.bounded[Int](100)
      ref   <- Ref.Synchronized.make(0)
      _     <- queue.offer(1).forever.fork
      error <- shard(queue, 10, makeWorker(ref))
      _     <- printLine(s"Failed with ${error}")
    } yield ()).exitCode
  }
}
