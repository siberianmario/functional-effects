package net.degoes.zio

import zio._

import scala.io.Source

object SimpleActor extends App {
  import zio.Console._

  sealed trait Command
  case object ReadTemperature                       extends Command
  final case class AdjustTemperature(value: Double) extends Command

  type TemperatureActor = Command => Task[Double]

  /**
   * EXERCISE
   *
   * Using ZIO Queue and Promise, implement the logic necessary to create an
   * actor as a function from `Command` to `Task[Double]`.
   */
  def makeActor(initialTemperature: Double): UIO[TemperatureActor] = {
    type Bundle = (Command, Promise[Nothing, Double])

    var temperature: Double = initialTemperature

    for {
      q <- ZQueue.unbounded[Bundle]
      _ <- q.take.flatMap {
            case (ReadTemperature, p) =>
              p.succeed(temperature)
            case (AdjustTemperature(temp), p) =>
              temperature += temp
              p.succeed(temperature)
          }.forever.fork
    } yield (cmd: Command) =>
      Promise
        .make[Nothing, Double]
        .flatMap(p => q.offer(cmd -> p) *> p.await)
  }

  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] = {
    val temperatures = (0 to 100).map(_.toDouble)

    (for {
      actor <- makeActor(0)
      _     <- ZIO.foreachPar(temperatures)(temp => actor(AdjustTemperature(temp)))
      temp  <- actor(ReadTemperature)
      _     <- printLine(s"Final temperature is ${temp}")
    } yield ()).exitCode
  }
}

object parallel_web_crawler extends App {
  import zio.Console._

  trait Web {
    def getURL(url: URL): IO[Exception, String]
  }
  object Web {

    /**
     * EXERCISE
     *
     * Implement a layer for `Web` that uses the `ZIO.attemptBlockingIO` combinator
     * to safely wrap `Source.fromURL` into a functional effect.
     */
    val live: ZLayer[Any, Nothing, Has[Web]] = ZLayer.succeed { (url: URL) =>
      ZManaged
        .fromAutoCloseable(ZIO.attemptBlockingIO(Source.fromURL(url.url)))
        .use(s => ZIO.attemptBlockingIO(s.mkString))
    }
  }

  /**
   * EXERCISE
   *
   * Using `ZIO.accessM`, delegate to the `Web` module's `getURL` function.
   */
  def getURL(url: URL): ZIO[Has[Web], Exception, String] = ZIO.serviceWith[Web](_.getURL(url))

  final case class CrawlState[+E](visited: Set[URL], errors: List[E]) {
    def visitAll(urls: Set[URL]): CrawlState[E] = copy(visited = visited ++ urls)

    def logError[E1 >: E](e: E1): CrawlState[E1] = copy(errors = e :: errors)
  }

  /**
   * EXERCISE
   *
   * Implement the `crawl` function using the helpers provided in this object.
   *
   * {{{
   * def getURL(url: URL): ZIO[Blocking, Exception, String]
   * def extractURLs(root: URL, html: String): List[URL]
   * }}}
   */
  def crawl(
    seeds: Set[URL],
    router: URL => Set[URL],
    processor: (URL, String) => IO[Exception, Unit],
    parallelism: Int = 10
  ): ZIO[Has[Web] with Has[Console], Nothing, List[Exception]] = {
    val emptySet = ZIO.succeed(Set.empty[URL])

    def loop(seeds: Set[URL], ref: Ref[CrawlState[Exception]]): ZIO[Has[Web] with Has[Console], Nothing, Unit] =
      if (seeds.isEmpty)
        ZIO.unit
      else
        ref.getAndUpdate(_.visitAll(seeds)).flatMap { state =>
          ZIO.foreachParNDiscard(parallelism)(seeds -- state.visited) { url =>
            printLine(s"requesting $url").orDie *>
              getURL(url)
                .tap(processor(url, _))
                .map(extractURLs(url, _).toSet)
                .map(_.flatMap(router))
                .catchAll(e => ref.update(_.logError(e)) *> emptySet)
                .flatMap(loop(_, ref))
          }
        }

    for {
      ref   <- Ref.make[CrawlState[Exception]](CrawlState(Set.empty, Nil))
      _     <- loop(seeds, ref)
      state <- ref.get
    } yield state.errors
  }

  /**
   * A data structure representing a structured URL, with a smart constructor.
   */
  final case class URL private (parsed: io.lemonlabs.uri.AbsoluteUrl) {
    import io.lemonlabs.uri._

    def relative(page: String): Option[URL] =
      Url.parseOption(page).collect {
        case RelativeUrl(path @ AbsolutePath(_), _, _) =>
          new URL(parsed.withPath(path))

        case RelativeUrl(RootlessPath(parts @ head +: tail), _, _) =>
          val whole =
            if (head.forall(_ == '.'))
              parsed.path.parts.dropRight(head.length) ++ tail
            else
              parsed.path.parts ++ parts
          new URL(parsed.withPath(UrlPath(whole)))
      }

    def url: String = parsed.toString

    override def equals(a: Any): Boolean = a match {
      case that: URL => this.url == that.url
      case _         => false
    }

    override def hashCode: Int = url.hashCode
  }

  object URL {
    import io.lemonlabs.uri._

    def make(url: String): Option[URL] =
      AbsoluteUrl.parseOption(url).map(new URL(_))
  }

  /**
   * A function that extracts URLs from a given web page.
   */
  def extractURLs(root: URL, html: String): List[URL] = {
    val pattern = "href=[\"\']([^\"\']+)[\"\']".r

    scala.util
      .Try({
        val matches = pattern.findAllMatchIn(html).map(_.group(1)).toList

        for {
          m   <- matches
          url <- URL.make(m).toList ++ root.relative(m).toList
        } yield url
      })
      .getOrElse(Nil)
  }

  object test {
    val Home          = URL.make("https://zio.dev").get
    val Index         = URL.make("https://zio.dev/index.html").get
    val ScaladocIndex = URL.make("https://zio.dev/scaladoc/index.html").get
    val About         = URL.make("https://zio.dev/about").get

    val SiteIndex =
      Map(
        Home          -> """<html><body><a href="/index.html">Home</a><a href="/scaladoc/index.html">Scaladocs</a></body></html>""",
        Index         -> """<html><body><a href="/index.html">Home</a><a href="/scaladoc/index.html">Scaladocs</a></body></html>""",
        ScaladocIndex -> """<html><body><a href="/index.html">Home</a><a href="/about">About</a></body></html>""",
        About         -> """<html><body><a href="/home.html">Home</a><a href="http://google.com">Google</a></body></html>"""
      )

    /**
     * EXERCISE
     *
     * Implement a test layer using the SiteIndex data.
     */
    val TestLayer: ZLayer[Any, Nothing, Has[Web]] = ZLayer.succeed((url: URL) =>
      ZIO.fromOption(SiteIndex.get(url)).mapError(_ => new Exception(s"failed to get content of $url"))
    )

    val TestRouter: URL => Set[URL] =
      url => if (url.parsed.apexDomain.contains("zio.dev")) Set(url) else Set()

    val Processor: (URL, String) => IO[Exception, Unit] =
      (url, html) => ZIO.unit
  }

  def printErrors[E](errors: List[E]): URIO[Has[Console], Unit] =
    (printLine("errors:") *> printLine(errors.mkString("\n"))).orDie

  /**
   * EXERCISE
   *
   * Run your test crawler using the test data, supplying it the custom layer
   * it needs.
   */
  def run(args: List[String]) = {
    import test._

    crawl(Set(Home), TestRouter, Processor)
      .provideSomeLayer[Has[Console]](Web.live) // TestLayer | Web.live
      .flatMap(printErrors)
      .exitCode
  }
}

object Hangman extends App {
  import Dictionary.Dictionary
  import GuessResult._
  import zio.Console._
  import zio.Random._

  import java.io.IOException

  /**
   * EXERCISE
   *
   * Implement an effect that gets a single, lower-case character from
   * the user.
   */
  lazy val getChoice: ZIO[Has[Console], IOException, Char] =
    (print("\nchoose a letter: ") *> readLine)
      .repeatUntil(s => s.length == 1 && s.head.isLetter)
      .map(_.head.toLower)

  /**
   * EXERCISE
   *
   * Implement an effect that prompts the user for their name, and
   * returns it.
   */
  lazy val getName: ZIO[Has[Console], IOException, String] =
    printLine("What is your name?") *> readLine

  /**
   * EXERCISE
   *
   * Implement an effect that chooses a random word from the dictionary.
   * The dictionary is `Dictionary.Dictionary`.
   */
  lazy val chooseWord: ZIO[Has[Random], Nothing, String] =
    nextIntBounded(Dictionary.length).map(Dictionary.apply)

  /**
   * EXERCISE
   *
   * Implement the main game loop, which gets choices from the user until
   * the game is won or lost.
   */
  def gameLoop(oldState: State): ZIO[Has[Console], IOException, Unit] =
    getChoice.flatMap { ch =>
      val newState = oldState.addChar(ch)
      val result   = analyzeNewInput(oldState, newState, ch)
      renderState(newState).as(result).flatMap {
        case Won =>
          printLine(s"Congratulations ${newState.name}! You won") *> ZIO.unit
        case Lost =>
          printLine(s"Sorry ${newState.name}, you lost") *> ZIO.unit
        case Correct =>
          printLine("Correct!") *> gameLoop(newState)
        case Incorrect =>
          printLine(s"Wrong letter! You have ${10 - newState.failures} attempt(s) left") *> gameLoop(newState)
        case Unchanged =>
          printLine("You have already tried this letter, choose another one") *> gameLoop(newState)
      }
    }

  def renderState(state: State): ZIO[Has[Console], IOException, Unit] = {

    /**
     *
     *  f     n  c  t  o
     *  -  -  -  -  -  -  -
     *
     *  Guesses: a, z, y, x
     *
     */
    val word =
      state.word.toList
        .map(c => if (state.guesses.contains(c)) s" $c " else "   ")
        .mkString("")

    val line = List.fill(state.word.length)(" - ").mkString("")

    val guesses = " Guesses: " + state.guesses.mkString(", ")

    val text = word + "\n" + line + "\n\n" + guesses + "\n"

    printLine(text)
  }

  final case class State(name: String, guesses: Set[Char], word: String) {
    def failures: Int = (guesses -- word.toSet).size

    def playerLost: Boolean = failures > 10

    def playerWon: Boolean = (word.toSet -- guesses).isEmpty

    def addChar(char: Char): State = copy(guesses = guesses + char)
  }

  sealed trait GuessResult
  object GuessResult {
    case object Won       extends GuessResult
    case object Lost      extends GuessResult
    case object Correct   extends GuessResult
    case object Incorrect extends GuessResult
    case object Unchanged extends GuessResult
  }

  def analyzeNewInput(
    oldState: State,
    newState: State,
    char: Char
  ): GuessResult =
    if (oldState.guesses.contains(char)) GuessResult.Unchanged
    else if (newState.playerWon) GuessResult.Won
    else if (newState.playerLost) GuessResult.Lost
    else if (oldState.word.contains(char)) GuessResult.Correct
    else GuessResult.Incorrect

  /**
   * EXERCISE
   *
   * Execute the main function and verify your program works as intended.
   */
  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] = {
    for {
      name  <- getName
      word  <- chooseWord
      state = State(name, Set(), word)
      _     <- renderState(state)
      _     <- gameLoop(state)
    } yield ()
  }.exitCode
}

object TicTacToe extends App {
  import zio.Console._
  import zio.Random._

  import scala.util.Try

  sealed trait Mark {
    val next: Mark

    final def renderChar: Char = this match {
      case Mark.X => 'X'
      case Mark.O => 'O'
    }
    final def render: String = renderChar.toString
  }
  object Mark {
    case object X extends Mark {
      override val next: Mark = Mark.O
    }
    case object O extends Mark {
      override val next: Mark = Mark.X
    }
  }

  final case class Board private (value: Vector[Vector[Option[Mark]]]) {

    /**
     * Retrieves the mark at the specified row/col.
     */
    def get(row: Int, col: Int): Option[Mark] =
      value.lift(row).flatMap(_.lift(col)).flatten

    /**
     * Places a mark on the board at the specified row/col.
     */
    def place(row: Int, col: Int, mark: Mark): Option[Board] =
      if (row >= 0 && col >= 0 && row < 3 && col < 3 && get(row, col).isEmpty)
        Some(
          copy(value = value.updated(row, value(row).updated(col, Some(mark))))
        )
      else None

    /**
     * Checks if board is finished and no other marks can be placed
     */
    def isFinished: Boolean = value.forall(_.forall(_.nonEmpty))

    /**
     * Renders the board to a string.
     */
    def render: String =
      value
        .map(_.map(_.fold(" ")(_.render)).mkString(" ", " | ", " "))
        .mkString("\n---|---|---\n")

    /**
     * Returns which mark won the game, if any.
     */
    def won: Option[Mark] =
      if (wonBy(Mark.X)) Some(Mark.X)
      else Some(Mark.O).filter(wonBy)

    private def wonBy(mark: Mark): Boolean =
      wonBy(0, 0, 1, 1, mark) ||
        wonBy(0, 2, 1, -1, mark) ||
        wonBy(0, 0, 0, 1, mark) ||
        wonBy(1, 0, 0, 1, mark) ||
        wonBy(2, 0, 0, 1, mark) ||
        wonBy(0, 0, 1, 0, mark) ||
        wonBy(0, 1, 1, 0, mark) ||
        wonBy(0, 2, 1, 0, mark)

    private def wonBy(
      row0: Int,
      col0: Int,
      rowInc: Int,
      colInc: Int,
      mark: Mark
    ): Boolean =
      extractLine(row0, col0, rowInc, colInc).collect { case Some(v) => v }.toList == List
        .fill(3)(mark)

    private def extractLine(
      row0: Int,
      col0: Int,
      rowInc: Int,
      colInc: Int
    ): Iterable[Option[Mark]] =
      for {
        i <- 0 to 2
      } yield value(row0 + rowInc * i)(col0 + colInc * i)
  }
  object Board {
    final val empty = new Board(Vector.fill(3)(Vector.fill(3)(None)))

    def fromChars(
      first: Iterable[Char],
      second: Iterable[Char],
      third: Iterable[Char]
    ): Option[Board] =
      if (first.size != 3 || second.size != 3 || third.size != 3) None
      else {
        def toMark(char: Char): Option[Mark] =
          if (char.toLower == 'x') Some(Mark.X)
          else if (char.toLower == 'o') Some(Mark.O)
          else None

        Some(
          new Board(
            Vector(
              first.map(toMark).toVector,
              second.map(toMark).toVector,
              third.map(toMark).toVector
            )
          )
        )
      }
  }

  val TestBoard: String =
    Board
      .fromChars(
        List(' ', 'O', 'X'),
        List('O', 'X', 'O'),
        List('X', ' ', ' ')
      )
      .get
      .render

  val getMove: URIO[Has[Console], (Int, Int)] =
    (print("your move: ").orDie *>
      readLine.orDie.flatMap { input =>
        ZIO.fromTry {
          Try(input.strip.split(':').toList.map(_.toInt)).collect {
            case List(row, col) => (row, col)
          }
        }
      })
      .tapError(_ => printLine("expected format is `row:col`, please try again"))
      .eventually

  def playerMove(board: Board, player: Mark): URIO[Has[Console], Board] =
    getMove.flatMap {
      case (row, col) => ZIO.fromOption(board.place(row, col, player))
    }.tapError(_ => printLine("invalid move, please try again")).eventually

  def randomMove(board: Board, player: Mark): URIO[Has[Console] with Has[Random], Board] =
    (for {
      row <- nextIntBounded(3)
      col <- nextIntBounded(3)
      brd <- ZIO.fromOption(board.place(row, col, player))
      _   <- printLine(s"player ${player.render} moved $row:$col")
    } yield brd).eventually

  def loop(board: Board, player: Mark): RIO[Has[Console] with Has[Random], Unit] = {
    val newBoard = player match {
      case Mark.O =>
        playerMove(board, Mark.O)
      case Mark.X =>
        randomMove(board, Mark.X)
    }

    val nextMove = newBoard.flatMap(loop(_, player.next)).when(!board.isFinished)

    printLine(s"\n${board.render}") *> {
      if (board.isFinished)
        printLine("It's a draw, nobody wins!")
      else
        board.won.fold(nextMove)(winner => printLine(s"player ${winner.render} won"))
    }
  }

  /**
   * EXERCISE
   *
   * Implement a game of tic-tac-toe, where the player gets to play against a
   * computer opponent.
   */
  def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] =
    (printLine("Welcome to the tic-tac-toe game!\nYou are playing with `O`\nExpected move is `row:col`") *>
      loop(Board.empty, Mark.O)).exitCode
}
