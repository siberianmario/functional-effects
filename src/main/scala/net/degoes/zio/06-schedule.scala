package net.degoes.zio

import zio._
import zio.Console._

import java.io.IOException

object Retry extends App {


  /**
   * EXERCISE
   *
   * Using `Schedule.recurs`, create a schedule that recurs 5 times.
   */
  val fiveTimes: Schedule[Any, Any, Long] = Schedule.recurs(5)

  /**
   * EXERCISE
   *
   * Using the `ZIO.repeat`, repeat printing "Hello World" five times to the
   * console.
   */
  val repeated1: ZIO[ZEnv, IOException, Long] =
    printLine("Hello World").repeat(fiveTimes)

  /**
   * EXERCISE
   *
   * Using `Schedule.spaced`, create a schedule that recurs forever every 1 second.
   */
  val everySecond: Schedule[Any, Any, Long] = Schedule.spaced(1.second)

  /**
   * EXERCISE
   *
   * Using the `&&` method of the `Schedule` object, the `fiveTimes` schedule,
   * and the `everySecond` schedule, create a schedule that repeats fives times,
   * evey second.
   */
  val fiveTimesEverySecond: Schedule[Any, Any, (Long, Long)] = fiveTimes && everySecond

  /**
   * EXERCISE
   *
   * Using the `ZIO#repeat`, repeat the action printLine("Hi hi") using
   * `fiveTimesEverySecond`.
   */
  val repeated2: ZIO[ZEnv, IOException, (Long, Long)] =
    printLine("Hi hi").repeat(fiveTimesEverySecond)

  /**
   * EXERCISE
   *
   * Using `Schedule#andThen` the `fiveTimes` schedule, and the `everySecond`
   * schedule, create a schedule that repeats fives times rapidly, and then
   * repeats every second forever.
   */
  val fiveTimesThenEverySecond: Schedule[Any, Any, Long] = fiveTimes andThen everySecond

  /**
   * EXERCISE
   *
   * Using `ZIO#retry`, retry the following error a total of five times.
   */
  val error1: IO[String, Nothing]   = IO.fail("Uh oh!")
  val retried5: IO[String, Nothing] = error1.retryN(5)

  /**
   * EXERCISE
   *
   * Using the `Schedule#||`, the `fiveTimes` schedule, and the `everySecond`
   * schedule, create a schedule that repeats the minimum of five times and
   * every second.
   */
  val fiveTimesOrEverySecond: Schedule[Any, Any, (Long, Long)] = fiveTimes || everySecond

  /**
   * EXERCISE
   *
   * Using `Schedule.exponential`, create an exponential schedule that starts
   * from 10 milliseconds.
   */
  val exponentialSchedule: Schedule[Any, Any, zio.Duration] = Schedule.exponential(10.millis)

  // (effect orElse otherService).retry(exponentialSchedule).timeout(60.seconds)

  /**
   * EXERCISE
   *
   * Using `Schedule.jittered` produced a jittered version of `exponentialSchedule`.
   */
  val jitteredExponential: Schedule[Has[Random], Any, zio.Duration] = exponentialSchedule.jittered

  /**
   * EXERCISE
   *
   * Using `Schedule.whileOutput`, produce a filtered schedule from `Schedule.forever`
   * that will halt when the number of recurrences exceeds 100.
   */
  val oneHundred: Schedule[Any, Any, Long] = Schedule.forever.whileOutput(_ <= 100)

  /**
   * EXERCISE
   *
   * Using `Schedule.identity`, produce a schedule that recurs forever, without delay,
   * returning its inputs.
   */
  def inputs[A]: Schedule[Any, A, A] = Schedule.identity

  /**
   * EXERCISE
   *
   * Using `Schedule#collect`, produce a schedule that recurs forever, collecting its
   * inputs into a list.
   */
  def collectedInputs[A]: Schedule[Any, A, Chunk[A]] = Schedule.collectAll

  /**
   * EXERCISE
   *
   * Using  `*>` (`zipRight`), combine `fiveTimes` and `everySecond` but return
   * the output of `everySecond`.
   */
  val fiveTimesEverySecondR: Schedule[Any, Any, Long] = fiveTimes *> everySecond

  /**
   * EXERCISE
   *
   * Produce a jittered schedule that first does exponential spacing (starting
   * from 10 milliseconds), but then after the spacing reaches 60 seconds,
   * switches over to fixed spacing of 60 seconds between recurrences, but will
   * only do that for up to 100 times, and produce a list of the inputs to
   * the schedule.
   */
  import Schedule.{ exponential, fixed, identity, recurs }

  val exp: Schedule[Any, Any, Duration] = exponential(10.millis).whileOutput(_ < 60.seconds)

  val fix: Schedule[Any, Any, Long] = fixed(60.seconds) <* recurs(100)

  def mySchedule[A]: Schedule[ZEnv, A, Chunk[A]] =
    (identity[A] <* (exp ++ fix)).jittered.collectAll

  def run(args: List[String]) =
    printLine("Hello World")
      .repeat((Schedule.forever >>> mySchedule).tapOutput(printLine(_).orDie))
      .exitCode
}
